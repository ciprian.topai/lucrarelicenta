<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
        <title>Gym_fitness</title>

    </head>


    <body class="body-contact">

        <header>
            <a href="../home.php">
                Gym_fitness
                <i class="fas fa-walking"></i>
            </a>

            <i id="menu-btn" class="fas fa-bars" onclick="show()"></i>

            <nav class="navigation">
                <button class="btnLogin-popup"><a href="../index.php">Chat</a></button>
                
            </nav>

        </header>

        <div class="page-container">

            <aside id="side-menu">
                <a href="../home.php">Acasa</a>
                <a href="servicii.php">Servicii</a>
                <a href="galerie.php">Galerie</a>
                <a href="abonamente.php">Abonamente</a>
                <a href="contact.php">Contact</a>
                


            </aside>

            <main>
                <h2 class="title">Contact</h2>

                <div class="row">
                    <div class="col-50">
                        <h3>Contactează-ne dacă:</h3>

                        <ul class="contact-list">
                            <li>Doresti informații despre abonamente</li>
                            <li>Dorești informații despe nutritie</li>
                            <li>Ai sugestii/propuneri</li>
                        </ul>
                    </div>

                    <div class="col-50">
                         <h3>Date de contact</h3>

                         <ul class="contact-list">
                             <li><b>Adresa:</b> Aleea Studenților 5</li>
                             <li><b>Oraș:</b> Timișoara</li>
                             <li><b>Telefon:</b> 0734987546</li>
                             
                         </ul>
                    </div>

                </div>

                <div class="row">
                     <div class="col-50">
                        <iframe class="map"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2784.118416330984!2d21.236670515826393!3d45.
                                 74877192243017!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47455d8f581b5cf3%3A0x71bfb50fe74530ca!
                                 2sAleea%20Studen%C8%9Bilor%2C%20Timi%C8%99oara!5e0!3m2!1sro!2sro!4v1651918039215!5m2!1sro!2sro" 
                            style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                     </div>

                     <div class="col-50">
                        <div class="contact-form">
                            <form action="contactinformatii.php" method="POST">
                                <h3 class="centered">Lasa un mesaj</h3>
                                <input type="user" name="user" placeholder="Nume" />
                                <input type="email" name="email" placeholder="Email" />
                                <textarea rows="4" name="message" placeholder="Mesaj"></textarea>
                                <button  type="submit">Transmite </button>
                            </form>
                        </div>
                     </div>
                </div>
            </main>


        </div>

        <script src="../js/main.js"></script>
        <footer>
            <div class="footer-div">
              <div>
               <p class="copyright">Copyright &copy; 2023 by Ciprian</p>
              </div>
               <div class="footer-adresa">
                   <p class="footer-p">Adresa: Aleea Studenților 5</p>
                   <p class="footer-p"> Telefon: 0734987546 </p>
                   <p class="footer-p">Adresa email: gym_fit@yahoo.com</p>
               </div>
            </div>
           </footer>
    </body>

</html>