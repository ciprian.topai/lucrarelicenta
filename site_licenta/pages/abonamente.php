<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
        <title>Gym_fitness</title>

    </head>


    <body class="abonamente">

        <header>
            <a href="../home.php">
                Gym_fitness
                <i class="fas fa-walking"></i>
            </a>

            <i id="menu-btn" class="fas fa-bars" onclick="show()"></i>

            <nav class="navigation">
                <button class="btnLogin-popup"><a href="../index.php">Chat</a></button>
                
            </nav>

        </header>

        <div class="page-container">

            <aside id="side-menu">
                <a href="../home.php">Acasa</a>
                <a href="servicii.php">Servicii</a>
                <a href="galerie.php">Galerie</a>
                <a href="abonamente.php">Abonamente</a>
                <a href="contact.php">Contact</a>
                


            </aside>

            <main >
                <h2 class="title">Abonamente</h2>
                
                <div class="abonamente-table">
                    <table class="shadow">
                        <thead>
                        <tr>
                          <th>Abonament</th>
                          <th>Durata</th>
                          <th>Pret</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Fitness</th>
                            <td>1 sedinta</td>
                            <td>40 lei</td>
                        </tr>
                        <tr>
                            <th>Fitness Full Time</th>
                            <td>Nelimitat/luna</td>
                            <td>170</td>
                        </tr>
                        <tr>
                            <th>Fitness Full Time Elev/Student/Pensionar</th>
                            <td>Nelimitat/luna</td>
                            <td>150 lei</td>
                        </tr>
                        <tr>
                            <th>Personal Trainer</th>
                            <td>1 sedinta</td>
                            <td>90 lei</td>
                        </tr>
                        </tbody>
                    </table>
                    
                </div>

                <div class="abonamente-table">
                    <table class="shadow">
                        <thead>
                        <tr>
                          <th>Abonament</th>
                          <th>Durata</th>
                          <th>Pret</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Aerobic</th>
                            <td>1 sedinta</td>
                            <td>40 lei</td>
                        </tr>
                        <tr>
                            <th>Aerobic 8</th>
                            <td>8 sedinte/luna</td>
                            <td>170</td>
                        </tr>
                        <tr>
                            <th>Aerobic 12</th>
                            <td>12 sedinte/luna</td>
                            <td>200 lei</td>
                        </tr>
                        <tr>
                            <th>Activ Aerobic</th>
                            <td>nelimitat/luna</td>
                            <td>320 lei</td>
                        </tr>
                        <tr>
                            <th>Flexi combinat</th>
                            <td>12 sedinte</td>
                            <td>240 lei</td>
                        </tr>
                        <tr>
                            <th>Flexi combinat</th>
                            <td>8 sedinte</td>
                            <td>200 lei</td>
                        </tr>
                        </tbody>
                    </table>
                    
                </div>
                <div class="contact-form">
                            <form action="abonamentinfo.php" method="post">
                                <h3 class="centered">Abonament sală fitness</h3>
                                <input type="nume" name="nume" placeholder="Nume" />
                                <input type="prenume" name="prenume" placeholder="Prenume" />
                                <input type="abonament" name="abonament" placeholder="Abonament" />
                                <input type="durata" name="durata" placeholder="Durata" />
                                <button  type="submit">Transmite </button>
                            </form>
                </div>
                
           
            </main>

        </div>
       

        <script src="../js/main.js"></script>
        <footer>
            <div class="footer-div">
              <div>
               <p class="copyright">Copyright &copy; 2023 by Ciprian</p>
              </div>
               <div class="footer-adresa">
                   <p class="footer-p">Adresa: Aleea Studenților 5</p>
                   <p class="footer-p"> Telefon: 0734987546 </p>
                   <p class="footer-p">Adresa email: gym_fit@yahoo.com</p>
               </div>
            </div>
           </footer>
    </body> 
    
        
            


</html>