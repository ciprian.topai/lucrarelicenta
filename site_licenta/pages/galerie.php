<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
        <title>Gym_fitness</title>

    </head>


    <body class="galerie">

        <header>
            <a href="../home.php">
                Gym_fitness
                <i class="fas fa-walking"></i>
            </a>

            <i id="menu-btn" class="fas fa-bars" onclick="show()"></i>

            <nav class="navigation">
                <button class="btnLogin-popup"><a href="../index.php">Chat</a></button>
                
            </nav>

        </header>

        <div class="page-container">

            <aside id="side-menu">
                <a href="../home.php">Acasa</a>
                <a href="servicii.php">Servicii</a>
                <a href="galerie.php">Galerie</a>
                <a href="abonamente.php">Abonamente</a>
                <a href="contact.php">Contact</a>
                


            </aside>

            <main class="section-testimonials ">
                
                <div class="testimonials-container">
                 <h2 class="test-h2">Testmoniale</h2>
                <div class="testimonials">
                <figure class="testimonial">
                   <img class="testimonial-img" alt="O poza cu un client"
                   src="../images/testmonial1.jpeg"/>
                   <blockquote class="testimonial-text">
                    „Intr-un moment in care nu mai gaseam placere in nimic din jurul meu,
                    miscarea mi-a eliberat mintea,prizoniera a neincrederii in propria persoana, 
                    si astfel totul a devenit un stil de viata care graviteaza in jurul unui nucleu: 
                    sanatatea. Iar cel mai important pentru a evita sa te opresti undeva, cumva in drumul 
                    asta, este sa ai langa tine oamenii potriviti; iar coach-ul este unul dintre ei! 
                    Multumesc, Iulian!”
                   </blockquote>
                   <p class="testimonial-name">&mdash; Ana Maria Mihai</p>
                </figure>
                <figure class="testimonial">
                    <img class="testimonial-img" alt="O poza cu un client"
                    src="../images/testmonial2.jpg"/>
                    <blockquote class="testimonial-text">
                        „Am avut o colaborare frumoasa cu Iulian , cand mi am deschis prima scoala de dans 
                        el mi-a fost ca si un stalp de rezistenta a afacerii.Mereu optimist si cu dorinta 
                        de mai mult, Iulian este o persoana deosebita, de incredere Il recomand drept instructor, 
                        dar ca si prieten de nadejde. Iti urez mult succes in cariera ta!”
                    </blockquote>
                    <p class="testimonial-name">&mdash; Iulia Dobre</p>
                 </figure>
                 <figure class="testimonial">
                    <img class="testimonial-img" alt="O poza cu un client"
                    src="../images/testmonial3.jpg"/>
                    <blockquote class="testimonial-text">
                        „De fiecare data vin cu placere!”
                    </blockquote>
                    <p class="testimonial-name">&mdash; Mihai Achim</p>
                 </figure>
                 <figure class="testimonial">
                    <img class="testimonial-img" alt="O poza cu un client"
                    src="../images/testmonial4.jpeg"/>
                    <blockquote class="testimonial-text">
                        „De aproape doi ani vin zilnic la sala!”
                    </blockquote>
                    <p class="testimonial-name">&mdash; Iasmina Burca</p>
                 </figure>
                 </div>
                </div>
                
                
                <div>
                <h2 class="test-h2">Galerie</h2>
                <div class="gallery">
                    <figure class="gallery-item">
                    <img src="../images/gallery1.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery2.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery3.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery4.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery5.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery6.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery7.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery8.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery9.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery10.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery11.jpg" 
                    alt="poza de la sala" />
                </figure>
                <figure class="gallery-item">
                    <img src="../images/gallery12.jpg" 
                    alt="poza de la sala" />
                </figure>
            </div>
            </div>
            </main>


        </div>
         
        <script src="../js/main.js"></script>
        <footer>
            <div class="footer-div">
              <div>
               <p class="copyright">Copyright &copy; 2023 by Ciprian</p>
              </div>
               <div class="footer-adresa">
                   <p class="footer-p">Adresa: Aleea Studenților 5</p>
                   <p class="footer-p"> Telefon: 0734987546 </p>
                   <p class="footer-p">Adresa email: gym_fit@yahoo.com</p>
               </div>
            </div>
           </footer>
    </body> 
    

</html>