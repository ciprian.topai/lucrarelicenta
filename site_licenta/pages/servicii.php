<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
        <title>Gym_fitness</title>

    </head>


    <body class="servicii">

        <header>
            <a href="../home.php">
                Gym_fitness
                <i class="fas fa-walking"></i>
            </a>

            <i id="menu-btn" class="fas fa-bars" onclick="show()"></i>

            <nav class="navigation">
                <button class="btnLogin-popup"><a href="../index.php">Chat</a></button>
                
            </nav>

        </header>

        <div class="page-container">

            <aside id="side-menu">
                <a href="../home.php">Acasa</a>
                <a href="servicii.php">Servicii</a>
                <a href="galerie.php">Galerie</a>
                <a href="abonamente.php">Abonamente</a>
                <a href="contact.php">Contact</a>
                


            </aside>

            <main>
               <div class="servici-container">
                <div>
                    <h2 class="fitness-h1" >Fitness</h2>
                    <p class="servici-p">Fitness-ul reprezinta un mod de viaţă accesibil tuturor. 
                        Oricine poate deveni un practicant al fitnessului. 
                        Cele mai cunoscute activităţi fizice sunt: mersul, alergarea, 
                        diferitele forme de gimnastică, dansul, înotul, badmintonul, tenisul, 
                        baschetul, fotbalul, voleiul de plajă sau ski-ul. Termenul de fitness 
                        exprimă capacitatea de efort sau pregătire fizică a unui individ. 
                        Putem spune că fitness-ul reprezintă capacitatea de a efectua eficient orice 
                        activitate fizică, fără a epuiza complet resursele de energie necesare.
                    </p>
                </div>
                <div>
                    <img class="servici-img" src="../images/gallery_fitnes.jpg" 
                    alt="poza de la sala" />
                </div>
               </div>
            </main>
            <div class="servici-container">
                <div>
                    <h2 class="fitness-h2">Antrenor personal</h2>
                    <p class="servici-pp">In general sportul este pentru oricine, dar nu toti reusesc 
                        sa se tina de antrenamente si sa-si formeze un stil de viata practicandu-l, 
                        sub orice forma. Daca te regasesti printre cei care isi doresc o schimbare in 
                        acest sens, antrenamentul personalizat este ceea ce te-ar putea ajuta. 
                        Antrenorul personal creaza o experienta unica, va fi atent la obiective dorite, 
                        este un bun coach, un excelent motivator si un profesionist aparte. Va fi atent 
                        sa ajungi la rezultatele  dorite, intr-un mod natural si sanatos, cu ajutorul  
                        unui plan de exercitii potrivit tie. Va gasi intotdeauna resurse pentru a te 
                        inspira si incuraja sa mergi mai departe. Te va ajuta sa deprinzi executia corecta 
                        a exercitiilor si va face ca antrenamentele tale sa fie distractive si amuzante, 
                        dar eficiente in acelasi timp.
                        Lucrand dupa un progam bine definit, un program aranjat pentru tine, 
                        vei avea rezultate bune si nu vei ramane la acelasi punct.
                    </p>
                </div>
                <div>
                    <img class="servici-img1" src="../images/gallery_antrenor.jpg" 
                    alt="poza de la sala" />
                </div>
               </div>
            </main>
            
        </div>
        
        <script src="../js/main.js"></script>
        <footer>
            <div class="footer-div">
              <div>
               <p class="copyright">Copyright &copy; 2023 by Ciprian</p>
              </div>
               <div class="footer-adresa">
                   <p class="footer-p">Adresa: Aleea Studenților 5</p>
                   <p class="footer-p"> Telefon: 0734987546 </p>
                   <p class="footer-p">Adresa email: gym_fit@yahoo.com</p>
               </div>
            </div>
           </footer>
    </body>

</html>