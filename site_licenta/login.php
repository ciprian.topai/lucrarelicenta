<?php
   session_start();
   if(isset($_SESSION['unique_id'])){
    header("location: users.php");
   }
?>
<?php include_once "header.php"; ?>
<body>
    <div class="wrapper">
        <section class="form login">
            <header>Chat sală fitness</header>
            <button><a href="home.php">Acasa</a></button>
            <button><a href="pages/servicii.php">Servicii</a></button>
            <button><a href="pages/galerie.php">Galerie</a></button>
            <button><a href="pages/abonamente.php">Abonamente</a></button>
            <button><a href="pages/contact.php">Contact</a></button>
        <form action="#">
           <div class="error-txt"></div>
           <div class="field input"> 
            <label>Email Address</label>
            <input type="text" name="email" placeholder="Enter your email">
           </div>
           <div class="field input "> 
            <label>Password</label>
            <input type="password" name="password" placeholder="Enter your password">
            <i class="fas fa-eye"></i>
           </div>
          <div class="field button">
           <input type="submit" value="Continue to chat">
          </div>
           
        </form>
        <div class="link">Not yet signet up? <a href="index.php">Sign now</a></div>
        </section>
    </div>

    <script src="javascript/pass-show-hide.js"></script>
    <script src="javascript/login.js"></script>

</body>
</html>