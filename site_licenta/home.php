<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
        <title>Gym_fitness</title>

    </head>


    <body class="acasa">

        <header>
            <a href="home.php">
                Gym_fitness
                <i class="fas fa-walking"></i>
            </a>

            <i id="menu-btn" class="fas fa-bars" onclick="show()"></i>

            <nav class="navigation">
                
               <button class="btnLogin-popup"><a href="index.php">Chat</a></button>
                
            </nav>

            

        </header>

        <div class="page-container">

            <aside id="side-menu">
                <a href="home.php">Acasa</a>
                <a href="pages/servicii.php">Servicii</a>
                <a href="pages/galerie.php">Galerie</a>
                <a href="pages/abonamente.php">Abonamente</a>
                <a href="pages/contact.php">Contact</a>
                
            </aside>

            <main>
                
            
                <div class="hero">
                    <div class="hero-text-box">
                      <h1 class="heading-primary">Această sală este deschisă non-stop</h1>
                      <p class="hero-description">
                        Conține o sală de aerobic, două săli de fitness și bodybuilding
                        dotate cu echipamente de fitness și cardio de ultimă generație
                        și un cabinet de masaj.
                      </p>
                      <div class="delivered-fit">
                        <div class="delivered-imgs">
                            <img src="images/customer-1.jpg" alt="poza avatar"/>
                            <img src="images/customer-2.jpg" alt="poza avatar"/>
                            <img src="images/customer-3.jpg" alt="poza avatar"/>
                            <img src="images/customer-4.jpg" alt="poza avatar"/>
                            <img src="images/customer-5.jpg" alt="poza avatar"/>
                            <img src="images/customer-6.jpg" alt="poza avatar"/>

                        </div>
                        <p class="icon-text">
                           <span> 10,000+ </span>abonamente anual!
                        </p>
                      </div>
                    </div>
                    <div class="hero-img-box">
                        <img src="images/gallery0.png" class="hero-img" alt="Trei imagini combinate"/>
                    </div>
                </div>
             <div>
                <p>
                    <a class="btn_link"   href="pages/servicii.php">Serviciile noastre</a>
                
                </p>
             </div>

                <div class="gallery">
                    <a target="_blank" href="images/gallery01.jpeg">
                        <img src="images/gallery01.jpeg">

                    </a>
                    <a target="_blank" href="images/gallery02.jpeg">
                        <img src="images/gallery02.jpeg">

                    </a>
                    <a target="_blank" href="images/gallery03.jpeg">
                        <img src="images/gallery03.jpeg">

                    </a>
                    <a target="_blank" href="images/gallery04.jpeg">
                        <img src="images/gallery04.jpeg">

                    </a>
                </div>
                
                </body>
                
            </main>


        </div>
       
        <script src="js/main.js"></script>
        <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
        <footer>
         <div class="footer-div">
           <div>
            <p class="copyright">Copyright &copy; 2023 by Ciprian</p>
           </div>
            <div class="footer-adresa">
                <p class="footer-p">Adresa: Aleea Studenților 5</p>
                <p class="footer-p"> Telefon: 0734987546 </p>
                <p class="footer-p">Adresa email: gym_fit@yahoo.com</p>
            </div>
         </div>
        </footer>
    </body>
    

</html>