-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1:3306
-- Timp de generare: sept. 04, 2023 la 09:29 AM
-- Versiune server: 8.0.31
-- Versiune PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `chat`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `abonament`
--

DROP TABLE IF EXISTS `abonament`;
CREATE TABLE IF NOT EXISTS `abonament` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nume` varchar(255) NOT NULL,
  `prenume` varchar(255) NOT NULL,
  `abonament` varchar(255) NOT NULL,
  `durata` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Eliminarea datelor din tabel `abonament`
--

INSERT INTO `abonament` (`id`, `nume`, `prenume`, `abonament`, `durata`) VALUES
(3, 'Pop', 'Daniel', 'fitness', '8 sedinte');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `contactdata`
--

DROP TABLE IF EXISTS `contactdata`;
CREATE TABLE IF NOT EXISTS `contactdata` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Eliminarea datelor din tabel `contactdata`
--

INSERT INTO `contactdata` (`id`, `user`, `email`, `message`) VALUES
(2, 'ciprian', 'william@gmail.com', 'LIKE');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `msg_id` int NOT NULL AUTO_INCREMENT,
  `incoming_msg_id` int NOT NULL,
  `outgoing_msg_id` int NOT NULL,
  `msg` varchar(1000) NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Eliminarea datelor din tabel `messages`
--

INSERT INTO `messages` (`msg_id`, `incoming_msg_id`, `outgoing_msg_id`, `msg`) VALUES
(74, 515264517, 1556053370, 'Hello'),
(75, 986365077, 1556053370, 'Hello'),
(76, 1101856847, 1556053370, 'Hello'),
(77, 515264517, 1101856847, 'Hy'),
(78, 1556053370, 1101856847, 'Hy'),
(79, 986365077, 1101856847, 'Hy'),
(80, 1556053370, 515264517, 'Bună'),
(81, 986365077, 515264517, 'Bună'),
(82, 1101856847, 515264517, 'Bună'),
(83, 515264517, 986365077, 'Hey'),
(84, 1556053370, 986365077, 'Hey'),
(85, 1101856847, 986365077, 'Hey'),
(86, 515264517, 1556053370, 'Ce faci?'),
(91, 986365077, 1556053370, 'Ce faci?'),
(92, 1101856847, 1556053370, 'Ce faci?'),
(93, 1556053370, 515264517, 'Pe acasă'),
(94, 1556053370, 515264517, 'Tu când mergi la sală?'),
(95, 515264517, 1556053370, 'Vreau să merg mâine'),
(96, 515264517, 1556053370, 'Tu mergi mâine?'),
(98, 1556053370, 515264517, 'DA');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `unique_id` int NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(400) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Eliminarea datelor din tabel `users`
--

INSERT INTO `users` (`user_id`, `unique_id`, `fname`, `lname`, `email`, `password`, `img`, `status`) VALUES
(8, 515264517, 'Pop', 'Daniel', 'william@gmail.com', '1234', '1690985401customer-4.jpg', 'Active now'),
(9, 1556053370, 'Burca', 'Roxana', 'roxana_90@yahoo.com', '1234', '1690985472matheus-ferrero-216385-scaled.jpg', 'Offline now'),
(10, 986365077, 'Banica', 'Andreea', 'andreea@yahoo.com', '1234', '1690985527michael-dam-258165-scaled.jpg', 'Offline now'),
(11, 1101856847, 'Tate', 'Ciprian', 'ciprian@yahoo.com', '1234', '1690985596customer-2.jpg', 'Offline now');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
